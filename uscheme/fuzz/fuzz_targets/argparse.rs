#![no_main]
#[macro_use] extern crate libfuzzer_sys;
extern crate uscheme;

use uscheme::argparse::ArgParse;
use std::str;

fuzz_target!(|data: &[u8]| {
    if let Ok(input) = str::from_utf8(data) {
        ArgParse::new()
            .filenames()
            .arg("foo", Some('f'), None)
            .arg("bar", Some('g'), None)
            .arg("baz", Some('b'), None)
            .arg("wallop", Some('w'), Some("wallop"))
            .parse(input);
    }
});