#![cfg_attr(feature = "no_std", no_std)]

#![cfg_attr(feature = "no_std", feature(
        lang_items,
        core_intrinsics
))]
#![feature(allocator_api)]

#[cfg(not(feature = "no_std"))]
extern crate core;

#[macro_use]
extern crate alloc;


// use alloc_system::System;

// #[global_allocator]
// static GLOBAL: System = System;

#[macro_use]
extern crate nom;

extern crate atoi;
extern crate checked;

extern crate num_integer;

// TODO: Do we really need itertools?
extern crate itertools;

#[cfg(test)]
extern crate proptest;
#[cfg(test)]
#[macro_use]
extern crate proptest_derive;



mod traits;
#[macro_use]
mod macros;
pub mod parse;
mod value;
mod environment;
mod evaluate;
pub mod backend;

#[cfg(test)]
mod testsuite;

pub mod list {
    pub type List<T> = ::alloc::vec::Vec<T>;
}
