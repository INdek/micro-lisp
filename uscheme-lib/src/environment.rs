use value::{Value, Error};
use alloc::string::String;
use core::default::Default;
use list::List;


pub type EnvironmentEntry = (String, Value);

#[derive(Debug)]
pub struct Environment {
    // This is purposefully not a HashMap
    // I want to test implementing a benchmark and seeing the performance difference
    vars: List<List<EnvironmentEntry>>,
}

impl Environment {
    pub fn new() -> Self {
        Default::default()
    }

    /// Returns the most local scope possible
    fn curr_scope_mut(&mut self) -> &mut List<EnvironmentEntry> {
        self.vars.last_mut().unwrap()
    }

    pub fn insert(&mut self, name: String, value: Value) {
        let curr_scope = self.curr_scope_mut();
        let val = curr_scope
            .iter()
            .position(|(n, _)| *n == name);

        match val {
            Some(index) => curr_scope[index] = (name, value),
            None => curr_scope.push((name, value)),
        }
    }

    pub fn lookup(&self, name: &String) -> &Value {
        self.lookup_opt(name)
            .unwrap_or(&Value::Error(Error::UnboundVariable))
    }

    pub fn lookup_opt(&self, name: &String) -> Option<&Value> {
        (0..self.vars.len())
            .rev() //Lookup from local to global scopes
            .fold(None, |acc, level| {
                acc.or_else(|| {
                    self.lookup_in_scope_opt(name, level)
                })
            })
    }

    fn lookup_in_scope_opt(&self, name: &String, level: usize) -> Option<&Value> {
        self.vars[level].iter()
            .find(|(n, _)| n == name)
            .map(|(_, value)| value)
    }

    pub fn push_scope(&mut self) {
        self.vars.push(List::new())
    }

    pub fn pop_scope(&mut self) -> Option<List<EnvironmentEntry>>{
        // We can't pop the global environment
        if self.vars.len() > 1 {
            self.vars.pop()
        } else {
            None
        }
    }
}

impl Default for Environment {
    fn default() -> Self {
        Environment {
            vars: list![
                list![] // Global scope
            ],
        }
    }
}

#[cfg(test)]
mod tests {
    macro_rules! env_test {
        ($name: ident, $body: expr) => {
            #[test]
            fn $name() {
                use super::*;

                let closure: &dyn Fn(Environment) = &$body;
                closure(Environment::new())
            }
        }
    }

    env_test!(store_lookup, |mut env| {
        let name: String = "test".into();
        env.insert(name.clone(), Value::Integer(1));
        assert_eq!(env.lookup(&name), &Value::Integer(1));
    });

    env_test!(store_replace, |mut env| {
        let name: String = "test".into();
        env.insert(name.clone(), Value::Integer(1));
        env.insert(name.clone(), Value::Integer(2));

        assert_eq!(env.lookup(&name), &Value::Integer(2));
    });

    env_test!(lookup_none, |env| {
        let name: String = "test".into();

        assert_eq!(env.lookup(&name), &Value::Error(Error::UnboundVariable));
    });

    env_test!(pop_global_scope, |mut env| {
        assert_eq!(env.pop_scope(), None);
    });

    env_test!(scoped_lookup, |mut env| {
        let name: String = "test".into();
        env.insert(name.clone(), Value::Integer(1));
        env.push_scope();
        env.insert(name.clone(), Value::Integer(2));
        assert_eq!(env.lookup(&name), &Value::Integer(2));
        env.pop_scope();
        assert_eq!(env.lookup(&name), &Value::Integer(1));
    });
}
