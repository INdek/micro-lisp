use alloc::vec::Vec;
use core;

use nom::types::CompleteByteSlice as Input;
use nom::{
    self,
    is_alphabetic,
    is_digit
};

macro_rules! char_if (
    ($i:expr, $c: expr) => ({
        use nom::lib::std::result::Result::*;
        use nom::lib::std::option::Option::*;
        use nom::{Err,Needed,ErrorKind,Context,need_more};

        use nom::Slice;
        use nom::AsChar;
        use nom::InputIter;

        match ($i).iter_elements().next().map(|c| {
            let chr = c.as_char(); // <== we need that later
            let b = ($c)(chr); // <== call fn instead of ==
            (b, chr) // <== need both now
        }) {
            None             => need_more($i, Needed::Size(1)),
            Some((false, _)) => { // <== specific char is irrelevant
                let e: ErrorKind<u32> = ErrorKind::Char;
                Err(Err::Error(Context::Code($i, e)))
            },
            Some((true,  c)) => Ok(( // <== use c to not do double work
                $i.slice(c.len()..), // <== calculating the length changed
                c // <== no more iter elements etc needed
            )),
        }
    });
);


pub type List<T> = Vec<T>;

pub type Boolean<'a> = &'a str;

pub type Identifier<'a> = &'a str;

pub type Symbol<'a> = &'a str;

pub type Integer<'a> = &'a str;

pub type Rational<'a> = (Integer<'a>, Integer<'a>);

pub type String<'a> = &'a str;

pub type Comment<'a> = &'a str;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Ast<'a> {
    Comment(Comment<'a>),
    String(String<'a>),
    Boolean(Boolean<'a>),
    Integer(Integer<'a>),
    Rational(Rational<'a>),
    Identifier(Identifier<'a>),
    Symbol(Symbol<'a>),
    Sexp(List<Ast<'a>>),
    Qexp(List<Ast<'a>>),
}

fn unsafe_utf8(v: Input) -> &str {
    unsafe {
        core::str::from_utf8_unchecked(v.0)
    }
}

named!(whitespace<Input, Input>, call!(nom::multispace));

// This function decides if a byte is a valid identifier character
// TODO: Some of these characters are probably next to each other in the
// ascii table
fn is_initial(input: u8) -> bool {
    // TODO: is_alphabetic eats up upper case characters
    is_alphabetic(input) ||
        input == b'!' ||
        input == b'$' ||
        input == b'%' ||
        input == b'&' ||
        input == b'*' ||
        input == b'/' ||
        input == b':' ||
        input == b'<' ||
        input == b'=' ||
        input == b'>' ||
        input == b'?' ||
        input == b'^' ||
        input == b'_' ||
        input == b'@' ||
        input == b'~'
}

fn is_special_subsequent(input: u8) -> bool {
    input == b'.' ||
    input == b'+' ||
    input == b'-'
}

fn is_subsequent(input: u8) -> bool {
    is_initial(input) || is_digit(input) || is_special_subsequent(input)
}

named!(peculiar_identifier<Input, Input>, alt!(
    tag!("+") |
    tag!("-") |
    tag!("...")
));

named!(identifier<Input, &str>, map!(alt!(
    recognize!(preceded!(
        char_if!(|c| is_initial(c as u8)),
        take_while!(is_subsequent)
    )) |
    peculiar_identifier
), unsafe_utf8));

named!(boolean<Input, Boolean>, map!(recognize!(preceded!(
    char!('#'),
    one_of!("tf")
)), unsafe_utf8));

named!(string<Input, String>, map_res!(map!(preceded!(
        char!('"'),
        take_until_and_consume!("\"")
), |b| b.0), core::str::from_utf8));

named!(comment<Input, Comment>, map_res!(map!(preceded!(
        char!(';'),
        take_until_and_consume!("\n")
), |b| b.0), core::str::from_utf8));

named!(integer<Input, Integer>, map!(recognize!(preceded!(
    opt!(char!('-')),
    take_while1!(is_digit)
)), unsafe_utf8));


named!(rational<Input, Rational>, do_parse!(
    num: integer >>
    tag!("/") >>
    denom: integer >>
    ((num, denom))
));


named!(sexpression<Input, Vec<Ast>>, delimited!(
    terminated!(char!('('), opt!(whitespace)),
    separated_list!(whitespace, complete!(ast)),
    preceded!(opt!(whitespace), char!(')'))
));

named!(qexpression<Input, Vec<Ast>>, preceded!(
    terminated!(char!('\''), opt!(whitespace)),
    sexpression
));

named!(symbol<Input, Symbol>, preceded!(char!('\''), identifier));


named!(ast<Input, Ast>, alt_complete!(
        map!(comment, Ast::Comment) |
        map!(string, Ast::String) |
        map!(boolean, Ast::Boolean) |
        map!(rational, Ast::Rational) |
        map!(integer, Ast::Integer) |
        map!(identifier, Ast::Identifier) |
        map!(symbol, Ast::Symbol) |
        map!(sexpression, Ast::Sexp) |
        map!(qexpression, Ast::Qexp)
));

named!(multi_ast<Input, List<Ast>>, delimited!(
    opt!(whitespace),
    separated_list!(whitespace, complete!(ast)),
    opt!(whitespace)
));

pub fn parse_single(input: &[u8]) -> Result<(Input, Ast), nom::Err<Input>> {
    ast(Input(input))
}

pub fn parse(input: &[u8]) -> Result<(Input, List<Ast>), nom::Err<Input>> {
    multi_ast(Input(input))
}


// TODO: Parse quote test
// We currently can't parse this: '1
#[cfg(test)]
mod tests {
    use super::Ast::*;
    use super::*;

    mod whitespace {
        use super::*;

        parse_test!(allowed_chars, whitespace, b"  \n\r\t", Input(b"  \n\r\t"));
    }


    mod name {
        use super::*;

        parse_test!(parse_identifier, identifier, b"test", "test");
        parse_test!(parse_identifier_num, identifier, b"test123", "test123");
        parse_test!(parse_identifier_divide, identifier, b"/", "/");
        parse_test!(parse_identifier_minus, identifier, b"-", "-");
        parse_test!(parse_identifier_excl, identifier, b"ident!", "ident!");
        parse_test!(parse_identifier_dollar_sign, identifier, b"ident$", "ident$");
        parse_test!(parse_identifier_percent, identifier, b"ident%", "ident%");
        parse_test!(parse_identifier_and, identifier, b"ident&", "ident&");
        parse_test!(parse_identifier_times, identifier, b"ident*", "ident*");
        parse_test!(parse_identifier_plus, identifier, b"ident+", "ident+");
        parse_test!(parse_identifier_dot, identifier, b"ident.", "ident.");
        parse_test!(parse_identifier_colon, identifier, b"ident:", "ident:");
        parse_test!(parse_identifier_less_than, identifier, b"ident<", "ident<");
        parse_test!(parse_identifier_equal, identifier, b"ident=", "ident=");
        parse_test!(parse_identifier_gt, identifier, b"ident>", "ident>");
        parse_test!(parse_identifier_question, identifier, b"ident?", "ident?");
        parse_test!(parse_identifier_at, identifier, b"ident@", "ident@");
        parse_test!(parse_identifier_xor, identifier, b"ident^", "ident^");
        parse_test!(parse_identifier_underscore, identifier, b"ident_", "ident_");
        parse_test!(parse_identifier_tilde, identifier, b"ident~", "ident~");
    }

    mod symbol {
        use super::*;

        parse_test!(parse_symbol_simple, symbol, b"'a", "a");
        parse_test!(parse_symbol_2, symbol, b"'abc", "abc");
    }

    mod integer {
        use super::*;

        parse_test!(parse_integer, integer, b"22", "22");
        parse_test!(parse_neg_integer, integer, b"-299", "-299");
    }

    mod rational {
        use super::*;

        parse_test!(simple, rational, b"1/1", ("1", "1"));
        parse_test!(negative, rational, b"-1/1", ("-1", "1"));
        parse_test!(double_negative, rational, b"-1/-1", ("-1", "-1"));
    }

    mod boolean {
        use super::*;

        parse_test!(parse_boolean_t, boolean, b"#t", "#t");
        parse_test!(parse_boolean_f, boolean, b"#f", "#f");
    }

    mod string {
        use super::*;

        parse_test!(normal, string, b"\"\"", "");
        parse_test!(chars, string, b"\"asdasd\"", "asdasd");
    }

    mod comment {
        use super::*;

        parse_test!(normal, comment, b"; oasdk aksdu \n", " oasdk aksdu ");
    }


    mod ast {
        use super::*;

        parse_test!(parse_ast_integer, ast, b"22", Integer("22"));
        parse_test!(parse_ast_rational, ast, b"22/2", Rational(("22", "2")));
        parse_test!(parse_ast_symbol, ast, b"'asd", Symbol("asd"));
        parse_test!(parse_ast_sexp_simple, ast, b"()", Sexp(Vec::new()));


        parse_test!(parse_list, ast, b"'(1 2 3)", Qexp(vec![
            Integer("1"),
            Integer("2"),
            Integer("3")
        ]));

        parse_test!(parse_list_space, ast, b"' ( 1 )", Qexp(vec![
            Integer("1"),
        ]));

        parse_test!(parse_sexp, ast, b"(+ 1 2 3)", Sexp(vec![
            Identifier("+"),
            Integer("1"),
            Integer("2"),
            Integer("3")
        ]));

        parse_test!(parse_sexp_space_end, ast, b"(+ 1 )", Sexp(vec![
            Identifier("+"),
            Integer("1")
        ]));

        parse_test!(parse_sexp_space_begin, ast, b"( + 1 )", Sexp(vec![
            Identifier("+"),
            Integer("1")
        ]));
    }

    mod full {
        use super::*;

        parse_test!(simple: comments_and_multispace, multi_ast, r#"
            ; this is a comment
            (+ 1 2) ; this is also a comment
            ; this too is a comment
        "#.as_bytes());
    }

    mod multi_parse {
        use super::*;

        #[test]
        fn test_multi_parse() {
            let source = r#"
                (+ 1 2)
                (+ 3 4)
            "#.as_bytes();
            let ast_res = parse(source);
            assert_eq!(ast_res, Ok((Input(&vec![]), vec![
                Sexp(vec![Identifier("+"), Integer("1"), Integer("2")]),
                Sexp(vec![Identifier("+"), Integer("3"), Integer("4")]),
            ])));
        }
    }
}