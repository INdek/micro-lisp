use proptest::prelude::*;
use value::{
  Value::{self, Integer, Sexp, Qexp, Identifier, Symbol, Function, Boolean},
  Error,
};

mod quote {
    use super::*;

    // eval_test!(symbol, Qexp(list![]))

    // eval_test!(symbol, Qexp(Sexp(list![
    //     Integer(1)
    // ])), b"'(1)");

    // eval_test!(quote, Qexp(Sexp(list![
    //     Integer(1)
    // ])), b"(quote (1))");
}

mod conditionals {
    use super::*;

    mod op_if {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(if)");
        eval_test!(one_arg, Value::Error(Error::ArityError), b"(if 1)");

        eval_test!(test_true, Integer(1), b"(if #t 1)");
        eval_test!(test_false, Integer(2), b"(if #f 1 2)");

        eval_test!(test_rest, Integer(3), b"(if #f 1 2 3)");
    }
}

mod definitions {
    use super::*;

    mod define {
        use super::*;

        // Define should return the identifier
        eval_test!(def_return, Identifier("test".into()), b"(define test 1)");

        // eval_test!(def_eval2, Integer(1), r#"
        //     (define test 1)
        //     test
        // "#.as_bytes());

        // TODO: This can use the simple_test macro
        #[test]
        fn def_eval() {
            use ::*;
            use value::Value::Integer;

            let mut backend = backend::Backend::new();
            backend.eval_ast(parse::parse_single(b"(define test 1)").unwrap().1).unwrap();
            let v = backend.eval_ast(parse::parse_single(b"test").unwrap().1).unwrap().unwrap();
            assert_eq!(v, Integer(1));
        }


        // eval_test!(def_return_function_form, Integer(10), r#"
        //     ((lambda (x) x) 10)
        // "#.as_bytes());
    }
}

mod equivalence {
    use super::*;

    mod op_equal {
        use super::*;

        //Scheme provides three primitives for equality and identity testing:

        // eq? is pointer comparison. It returns #t iff its arguments literally refer to the same objects in memory. Symbols are unique ('fred always evaluates to the same object). Two symbols that look the same are eq. Two variables that refer to the same object are eq.
        // eqv? is like eq? but does the right thing when comparing numbers. eqv? returns #t iff its arguments are eq or if its arguments are numbers that have the same value. eqv? does not convert integers to floats when comparing integers and floats though.
        // equal? returns true if its arguments have the same structure. Formally, we can define equal? recursively. equal? returns #t if its arguments are eqv, or if its arguments are lists whose corresponding elements are equal; and otherwise false. (Note the recursion.) Two objects that are eq are both eqv and equal. Two objects that are eqv are equal, but not necessarily eq. Two objects that are equal are not necessarily eqv or eq. eq is sometimes called an identity comparison and equal is called an equality comparison.

        eval_test!(equal_symbol, Boolean(true), b"(equal? 'a 'a)");
        eval_test!(equal_symbol_false, Boolean(false), b"(equal? 'a 'b)");
        eval_test!(equal_bool, Boolean(true), b"(equal? #t #t)");
        eval_test!(equal_bool_false, Boolean(false), b"(equal? #t #f)");

        eval_test!(equal_list, Boolean(true), b"(equal? '(1 2 3) '(1 2 3))");
        eval_test!(equal_list_false, Boolean(false), b"(equal? '(1 2 4) '(1 2 3))");

        eval_test!(equal_builtin, Boolean(true), b"(equal? car car)");
        eval_test!(equal_builtin_false, Boolean(false), b"(equal? car cdr)");

        eval_test!(equal_integer, Boolean(true), b"(equal? 1 1)");
        eval_test!(equal_integer_false, Boolean(false), b"(equal? 1 2)");

        eval_test!(equal_string, Boolean(true), br#"(equal? "asd" "asd")"#);
        eval_test!(equal_string_false, Boolean(false), br#"(equal? "asd" "abc")"#);

        eval_test!(equal_var_comparision, Boolean(true), br"
            (define a '(1 2 3))
            (equal? a '(1 2 3))
        ");

        eval_test!(equal_argument_count_3, Value::Error(Error::ArityError), b"(equal? 'a 'a 'a)");
        eval_test!(equal_argument_count_1, Value::Error(Error::ArityError), b"(equal? 'a)");
    }
}

mod integers {
    use super::*;

        mod add {
        use super::*;

        eval_test!(add, Integer(35), b"(+ 22 10 3)");
        eval_test!(add_fn, Value::Error(Error::InvalidOperation), b"(+ +)");

        eval_test!(add_empty, Integer(0), b"(+)");

        eval_test!(add_one_arg, Integer(3), b"(+ 3)");
        eval_test!(add_one_neg_arg, Integer(-4), b"(+ -4)");

        eval_test!(overflow, Value::Error(Error::IntegerOverflow), b"(+ 2147483647 100)");
    }

    mod sub {
        use super::*;

        eval_test!(sub, Integer(48), b"(- 60 10 2)");
        eval_test!(sub_single, Integer(-100), b"(- 100)");
        eval_test!(sub_empty, Value::Error(Error::ArityError), b"(-)");
        eval_test!(sub_one_arg, Integer(-2), b"(- 2)");

        eval_test!(underflow, Value::Error(Error::IntegerUnderflow), b"(- -2147483647 100)");
    }

    mod div {
        use super::*;

        eval_test!(div, Integer(15), b"(/ 60 2 2)");

        // TODO: Enable this when we have fp
        // eval_test!(div_one_arg, Integer(0.5), b"(/ 2)");

        eval_test!(div_0, Value::Error(Error::DivByZero), b"(/ 60 0)");
        eval_test!(div_empty, Value::Error(Error::ArityError), b"(/)");
    }

    mod mul {
        use super::*;

        eval_test!(mul, Integer(40), b"(* 10 2 2)");
        eval_test!(mul_empty, Integer(1), b"(*)");
        eval_test!(mul_one_arg, Integer(2), b"(* 2)");

        eval_test!(overflow, Value::Error(Error::IntegerOverflow), b"(* 5555 55 8  8 55 8  8)");
    }

    mod ord {
        use super::*;
        mod gt {
            use super::*;

            eval_test!(empty, Boolean(true), b"(>)");
            eval_test!(one_arg, Boolean(true), b"(> 1)");
            eval_test!(one_arg_neg, Boolean(true), b"(> -1)");
            eval_test!(two_arg_true, Boolean(true), b"(> 2 1)");
            eval_test!(two_arg_false, Boolean(false), b"(> 1 2)");
            eval_test!(two_arg_eq, Boolean(false), b"(> 1 1)");
            eval_test!(five_arg_true, Boolean(true), b"(> 5 4 3 2 1)");
            eval_test!(five_arg_false, Boolean(false), b"(> 5 4 10 2 1)");
        }

        mod gte {
            use super::*;

            eval_test!(empty, Boolean(true), b"(>=)");
            eval_test!(one_arg, Boolean(true), b"(>= 1)");
            eval_test!(one_arg_neg, Boolean(true), b"(>= -1)");
            eval_test!(two_arg_true, Boolean(true), b"(>= 2 1)");
            eval_test!(two_arg_false, Boolean(false), b"(>= 1 2)");
            eval_test!(two_arg_eq, Boolean(true), b"(>= 1 1)");
            eval_test!(five_arg_true, Boolean(true), b"(>= 5 4 3 2 1)");
            eval_test!(five_arg_false, Boolean(false), b"(>= 5 4 10 2 1)");
        }

        mod lt {
            use super::*;

            eval_test!(empty, Boolean(true), b"(<)");
            eval_test!(one_arg, Boolean(true), b"(< 1)");
            eval_test!(one_arg_neg, Boolean(true), b"(< -1)");
            eval_test!(two_arg_false, Boolean(false), b"(< 2 1)");
            eval_test!(two_arg_true, Boolean(true), b"(< 1 2)");
            eval_test!(two_arg_eq, Boolean(false), b"(< 1 1)");
            eval_test!(five_arg_true, Boolean(true), b"(< 1 2 3 4 5)");
            eval_test!(five_arg_false, Boolean(false), b"(< 1 2 10 3 4)");
        }

        mod lte {
            use super::*;

            eval_test!(empty, Boolean(true), b"(<=)");
            eval_test!(one_arg, Boolean(true), b"(<= 1)");
            eval_test!(one_arg_neg, Boolean(true), b"(<= -1)");
            eval_test!(two_arg_false, Boolean(false), b"(<= 2 1)");
            eval_test!(two_arg_true, Boolean(true), b"(<= 1 2)");
            eval_test!(two_arg_eq, Boolean(true), b"(<= 1 1)");
            eval_test!(five_arg_true, Boolean(true), b"(<= 1 2 3 4 5)");
            eval_test!(five_arg_false, Boolean(false), b"(<= 1 2 10 3 4)");
        }

        mod eq {
            use super::*;

            eval_test!(empty, Boolean(true), b"(=)");
            eval_test!(one_arg, Boolean(true), b"(= 1)");
            eval_test!(one_arg_neg, Boolean(true), b"(= -1)");
            eval_test!(all_eq, Boolean(true), b"(= -1 -1)");
            eval_test!(all_eq_2, Boolean(true), b"(= 1 1 1 1 1)");
            eval_test!(last_ne, Boolean(false), b"(= 1 1 1 1 2)");

            eval_test!(bool_cmp, Boolean(true), b"(= #t #t)");
            eval_test!(bool_cmp_diff, Boolean(false), b"(= #f #t)");

            eval_test!(str_cmp, Boolean(true), br#"(= "abc" "abc")"#);
        }
    }

    mod op_zero {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(zero?)");
        eval_test!(two_args, Value::Error(Error::ArityError), b"(zero? 1 2)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(zero? #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(zero? '())");

        eval_test!(returns_true_with_zero, Boolean(true), b"(zero? 0)");
        eval_test!(returns_false_with_one, Boolean(false), b"(zero? 1)");

        eval_test!(preevaluates_args, Boolean(false), b"(zero? (+ 0 1))");
    }

    mod op_positive {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(positive?)");
        eval_test!(two_args, Value::Error(Error::ArityError), b"(positive? 1 2)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(positive? #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(positive? '())");

        eval_test!(returns_false_with_negative, Boolean(false), b"(positive? -1)");
        eval_test!(returns_false_with_zero, Boolean(false), b"(positive? 0)");
        eval_test!(returns_true_with_one, Boolean(true), b"(positive? 1)");

        eval_test!(preevaluates_args, Boolean(true), b"(positive? (+ 0 1))");
    }

    mod op_negative {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(negative?)");
        eval_test!(two_args, Value::Error(Error::ArityError), b"(negative? 1 2)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(negative? #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(negative? '())");

        eval_test!(returns_true_with_negative, Boolean(true), b"(negative? -1)");
        eval_test!(returns_false_with_zero, Boolean(false), b"(negative? 0)");
        eval_test!(returns_false_with_one, Boolean(false), b"(negative? 1)");

        eval_test!(preevaluates_args, Boolean(false), b"(negative? (+ 0 1))");
    }

    mod op_odd {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(odd?)");
        eval_test!(two_args, Value::Error(Error::ArityError), b"(odd? 1 2)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(odd? #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(odd? '())");

        eval_test!(returns_true_with_one, Boolean(true), b"(odd? 1)");
        eval_test!(returns_false_with_zero, Boolean(false), b"(odd? 0)");
        eval_test!(returns_true_with_negative_one, Boolean(true), b"(odd? -1)");

        eval_test!(preevaluates_args, Boolean(true), b"(odd? (+ 0 1))");
    }

    mod op_even {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(even?)");
        eval_test!(two_args, Value::Error(Error::ArityError), b"(even? 1 2)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(even? #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(even? '())");

        eval_test!(returns_false_with_one, Boolean(false), b"(even? 1)");
        eval_test!(returns_true_with_zero, Boolean(true), b"(even? 0)");
        eval_test!(returns_false_with_negative_one, Boolean(false), b"(even? -1)");

        eval_test!(preevaluates_args, Boolean(false), b"(even? (+ 0 1))");
    }

    mod op_max {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(max)");
        eval_test!(one_arg, Value::Error(Error::ArityError), b"(max 1)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(max #f #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(max '() '())");

        eval_test!(returns_highest, Integer(2), b"(max 1 2)");
        eval_test!(returns_highest_4_elements, Integer(10), b"(max 1 2 3 10)");

        eval_test!(preevaluates_args, Integer(2), b"(max 1 (+ 1 1))");
    }

    mod op_min {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(min)");
        eval_test!(one_arg, Value::Error(Error::ArityError), b"(min 1)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(min #f #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(min '() '())");

        eval_test!(returns_lowest, Integer(1), b"(min 1 2)");
        eval_test!(returns_lowest_4_elements, Integer(1), b"(min 1 2 3 10)");

        eval_test!(preevaluates_args, Integer(2), b"(min 3 (+ 1 1))");
    }

    mod op_abs {
        use super::*;

        eval_test!(ignore: empty, Value::Error(Error::ArityError), b"(abs)");
        eval_test!(ignore: two_args, Value::Error(Error::ArityError), b"(abs 1 2)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(abs #f)");
        eval_test!(ignore: type_error_with_nil, Value::Error(Error::InvalidOperation), b"(abs '()");

        eval_test!(leaves_positive_args_unchanged, Integer(1), b"(abs 1)");
        eval_test!(leaves_0_unchanged, Integer(0), b"(abs 0)");
        eval_test!(flips_negative_signs, Integer(10), b"(abs -10)");

        // eval_test!(
        //     int_min,
        //     Integer(::value::Integer::min_value().abs()),
        //     format!("(abs {})", ::value::Integer::min_value())
        // );


        eval_test!(preevaluates_args, Integer(1), b"(abs (+ 1 -2))");

        proptest! {
            #[test]
            fn equivalent_to_rust_abs(i: ::value::Integer) {
                let mut backend = ::backend::Backend::new();

                let prog_src = format!("(abs {})", i);
                let prog = ::parse::parse(prog_src.as_bytes()).unwrap().1;
                let val = backend.eval_ast_list(prog).unwrap().unwrap();

                assert_eq!(val, Integer(i.abs()));
            }
        }
    }


    mod quotient {
        use super::*;

        eval_test!(empty, Value::Error(Error::ArityError), b"(quotient)");
        eval_test!(one_arg, Value::Error(Error::ArityError), b"(quotient 1)");

        eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(quotient #f #f)");
        eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(quotient '() '())");

        eval_test!(zero_denominator, Value::Error(Error::InvalidOperation), b"(quotient 1 0)");

        eval_test!(preevaluates_args, Integer(1), b"(quotient (+ 2 -1) 1)");

        eval_test!(basic, Integer(0), b"(quotient 1 2)");
        eval_test!(basic_2, Integer(1), b"(quotient 4 4)");
        eval_test!(negative_numerator, Integer(-1), b"(quotient -4 4)");
    }
}


#[cfg(feature = "rational-numbers")]
mod rationals {
    use super::super::value::rational;
    use super::*;

    macro_rules! rational {
        ($num: expr, $denom: expr) => {
            Value::Rational(rational::Rational::new($num, $denom))
        };
    }

    mod add {
        use super::*;

        eval_test!(add_rational, rational!(2, 1), b"(+ 1/1 1/1)");
        // eval_test!(add_fn, Value::Error(Error::InvalidOperation), b"(+ +)");

        // eval_test!(add_empty, Integer(0), b"(+)");

        // eval_test!(add_one_arg, Integer(3), b"(+ 3)");
        // eval_test!(add_one_neg_arg, Integer(-4), b"(+ -4)");


        // eval_test!(add_integer, rational!(4, 1), b"(+ 1 2/1)");
        // eval_test!(add_integer_reverse, rational!(5, 2), b"(+ 1/2 2)");



        proptest! {
            #[test]
            fn add_is_comutative(r: ::value::rational::Rational, i: i32) {
                equivalence_test!(body:
                    format!("(add {} {})", r, i),
                    format!("(add {} {})", i, r)
                );
            }
        }

        // eval_test!(overflow, Value::Error(Error::IntegerOverflow), b"(+ 2147483647 100)");
    }

    // mod sub {
    //     use super::*;

    //     eval_test!(sub, Integer(48), b"(- 60 10 2)");
    //     eval_test!(sub_single, Integer(-100), b"(- 100)");
    //     eval_test!(sub_empty, Value::Error(Error::ArityError), b"(-)");
    //     eval_test!(sub_one_arg, Integer(-2), b"(- 2)");

    //     eval_test!(underflow, Value::Error(Error::IntegerUnderflow), b"(- -2147483647 100)");
    // }

    // mod div {
    //     use super::*;

    //     eval_test!(div, Integer(15), b"(/ 60 2 2)");

    //     // TODO: Enable this when we have fp
    //     // eval_test!(div_one_arg, Integer(0.5), b"(/ 2)");

    //     eval_test!(div_0, Value::Error(Error::DivByZero), b"(/ 60 0)");
    //     eval_test!(div_empty, Value::Error(Error::ArityError), b"(/)");
    // }

    // mod mul {
    //     use super::*;

    //     eval_test!(mul, Integer(40), b"(* 10 2 2)");
    //     eval_test!(mul_empty, Integer(1), b"(*)");
    //     eval_test!(mul_one_arg, Integer(2), b"(* 2)");

    //     eval_test!(overflow, Value::Error(Error::IntegerOverflow), b"(* 5555 55 8  8 55 8  8)");
    // }

    // mod ord {
    //     use super::*;
    //     mod gt {
    //         use super::*;

    //         eval_test!(empty, Boolean(true), b"(>)");
    //         eval_test!(one_arg, Boolean(true), b"(> 1)");
    //         eval_test!(one_arg_neg, Boolean(true), b"(> -1)");
    //         eval_test!(two_arg_true, Boolean(true), b"(> 2 1)");
    //         eval_test!(two_arg_false, Boolean(false), b"(> 1 2)");
    //         eval_test!(two_arg_eq, Boolean(false), b"(> 1 1)");
    //         eval_test!(five_arg_true, Boolean(true), b"(> 5 4 3 2 1)");
    //         eval_test!(five_arg_false, Boolean(false), b"(> 5 4 10 2 1)");
    //     }

    //     mod gte {
    //         use super::*;

    //         eval_test!(empty, Boolean(true), b"(>=)");
    //         eval_test!(one_arg, Boolean(true), b"(>= 1)");
    //         eval_test!(one_arg_neg, Boolean(true), b"(>= -1)");
    //         eval_test!(two_arg_true, Boolean(true), b"(>= 2 1)");
    //         eval_test!(two_arg_false, Boolean(false), b"(>= 1 2)");
    //         eval_test!(two_arg_eq, Boolean(true), b"(>= 1 1)");
    //         eval_test!(five_arg_true, Boolean(true), b"(>= 5 4 3 2 1)");
    //         eval_test!(five_arg_false, Boolean(false), b"(>= 5 4 10 2 1)");
    //     }

    //     mod lt {
    //         use super::*;

    //         eval_test!(empty, Boolean(true), b"(<)");
    //         eval_test!(one_arg, Boolean(true), b"(< 1)");
    //         eval_test!(one_arg_neg, Boolean(true), b"(< -1)");
    //         eval_test!(two_arg_false, Boolean(false), b"(< 2 1)");
    //         eval_test!(two_arg_true, Boolean(true), b"(< 1 2)");
    //         eval_test!(two_arg_eq, Boolean(false), b"(< 1 1)");
    //         eval_test!(five_arg_true, Boolean(true), b"(< 1 2 3 4 5)");
    //         eval_test!(five_arg_false, Boolean(false), b"(< 1 2 10 3 4)");
    //     }

    //     mod lte {
    //         use super::*;

    //         eval_test!(empty, Boolean(true), b"(<=)");
    //         eval_test!(one_arg, Boolean(true), b"(<= 1)");
    //         eval_test!(one_arg_neg, Boolean(true), b"(<= -1)");
    //         eval_test!(two_arg_false, Boolean(false), b"(<= 2 1)");
    //         eval_test!(two_arg_true, Boolean(true), b"(<= 1 2)");
    //         eval_test!(two_arg_eq, Boolean(true), b"(<= 1 1)");
    //         eval_test!(five_arg_true, Boolean(true), b"(<= 1 2 3 4 5)");
    //         eval_test!(five_arg_false, Boolean(false), b"(<= 1 2 10 3 4)");
    //     }

    //     mod eq {
    //         use super::*;

    //         eval_test!(empty, Boolean(true), b"(=)");
    //         eval_test!(one_arg, Boolean(true), b"(= 1)");
    //         eval_test!(one_arg_neg, Boolean(true), b"(= -1)");
    //         eval_test!(all_eq, Boolean(true), b"(= -1 -1)");
    //         eval_test!(all_eq_2, Boolean(true), b"(= 1 1 1 1 1)");
    //         eval_test!(last_ne, Boolean(false), b"(= 1 1 1 1 2)");

    //         eval_test!(bool_cmp, Boolean(true), b"(= #t #t)");
    //         eval_test!(bool_cmp_diff, Boolean(false), b"(= #f #t)");

    //         eval_test!(str_cmp, Boolean(true), br#"(= "abc" "abc")"#);
    //     }
    // }

    // mod op_zero {
    //     use super::*;

    //     eval_test!(empty, Value::Error(Error::ArityError), b"(zero?)");
    //     eval_test!(two_args, Value::Error(Error::ArityError), b"(zero? 1 2)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(zero? #f)");
    //     eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(zero? '())");

    //     eval_test!(returns_true_with_zero, Boolean(true), b"(zero? 0)");
    //     eval_test!(returns_false_with_one, Boolean(false), b"(zero? 1)");

    //     eval_test!(preevaluates_args, Boolean(false), b"(zero? (+ 0 1))");
    // }

    // mod op_positive {
    //     use super::*;

    //     eval_test!(empty, Value::Error(Error::ArityError), b"(positive?)");
    //     eval_test!(two_args, Value::Error(Error::ArityError), b"(positive? 1 2)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(positive? #f)");
    //     eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(positive? '())");

    //     eval_test!(returns_false_with_negative, Boolean(false), b"(positive? -1)");
    //     eval_test!(returns_false_with_zero, Boolean(false), b"(positive? 0)");
    //     eval_test!(returns_true_with_one, Boolean(true), b"(positive? 1)");

    //     eval_test!(preevaluates_args, Boolean(true), b"(positive? (+ 0 1))");
    // }

    // mod op_negative {
    //     use super::*;

    //     eval_test!(empty, Value::Error(Error::ArityError), b"(negative?)");
    //     eval_test!(two_args, Value::Error(Error::ArityError), b"(negative? 1 2)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(negative? #f)");
    //     eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(negative? '())");

    //     eval_test!(returns_true_with_negative, Boolean(true), b"(negative? -1)");
    //     eval_test!(returns_false_with_zero, Boolean(false), b"(negative? 0)");
    //     eval_test!(returns_false_with_one, Boolean(false), b"(negative? 1)");

    //     eval_test!(preevaluates_args, Boolean(false), b"(negative? (+ 0 1))");
    // }

    // mod op_odd {
    //     use super::*;

    //     eval_test!(empty, Value::Error(Error::ArityError), b"(odd?)");
    //     eval_test!(two_args, Value::Error(Error::ArityError), b"(odd? 1 2)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(odd? #f)");
    //     eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(odd? '())");

    //     eval_test!(returns_true_with_one, Boolean(true), b"(odd? 1)");
    //     eval_test!(returns_false_with_zero, Boolean(false), b"(odd? 0)");
    //     eval_test!(returns_true_with_negative_one, Boolean(true), b"(odd? -1)");

    //     eval_test!(preevaluates_args, Boolean(true), b"(odd? (+ 0 1))");
    // }

    // mod op_even {
    //     use super::*;

    //     eval_test!(empty, Value::Error(Error::ArityError), b"(even?)");
    //     eval_test!(two_args, Value::Error(Error::ArityError), b"(even? 1 2)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(even? #f)");
    //     eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(even? '())");

    //     eval_test!(returns_false_with_one, Boolean(false), b"(even? 1)");
    //     eval_test!(returns_true_with_zero, Boolean(true), b"(even? 0)");
    //     eval_test!(returns_false_with_negative_one, Boolean(false), b"(even? -1)");

    //     eval_test!(preevaluates_args, Boolean(false), b"(even? (+ 0 1))");
    // }

    // mod op_max {
    //     use super::*;

    //     eval_test!(empty, Value::Error(Error::ArityError), b"(max)");
    //     eval_test!(one_arg, Value::Error(Error::ArityError), b"(max 1)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(max #f #f)");
    //     eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(max '() '())");

    //     eval_test!(returns_highest, Integer(2), b"(max 1 2)");
    //     eval_test!(returns_highest_4_elements, Integer(10), b"(max 1 2 3 10)");

    //     eval_test!(preevaluates_args, Integer(2), b"(max 1 (+ 1 1))");
    // }

    // mod op_min {
    //     use super::*;

    //     eval_test!(empty, Value::Error(Error::ArityError), b"(min)");
    //     eval_test!(one_arg, Value::Error(Error::ArityError), b"(min 1)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(min #f #f)");
    //     eval_test!(type_error_with_nil, Value::Error(Error::InvalidOperation), b"(min '() '())");

    //     eval_test!(returns_lowest, Integer(1), b"(min 1 2)");
    //     eval_test!(returns_lowest_4_elements, Integer(1), b"(min 1 2 3 10)");

    //     eval_test!(preevaluates_args, Integer(2), b"(min 3 (+ 1 1))");
    // }

    // mod op_abs {
    //     use super::*;

    //     eval_test!(ignore: empty, Value::Error(Error::ArityError), b"(abs)");
    //     eval_test!(ignore: two_args, Value::Error(Error::ArityError), b"(abs 1 2)");

    //     eval_test!(type_error_with_boolean, Value::Error(Error::InvalidOperation), b"(abs #f)");
    //     eval_test!(ignore: type_error_with_nil, Value::Error(Error::InvalidOperation), b"(abs '()");

    //     eval_test!(leaves_positive_args_unchanged, Integer(1), b"(abs 1)");
    //     eval_test!(leaves_0_unchanged, Integer(0), b"(abs 0)");
    //     eval_test!(flips_negative_signs, Integer(10), b"(abs -10)");

    //     // eval_test!(
    //     //     int_min,
    //     //     Integer(::value::Integer::min_value().abs()),
    //     //     format!("(abs {})", ::value::Integer::min_value())
    //     // );


    //     eval_test!(preevaluates_args, Integer(1), b"(abs (+ 1 -2))");

    //     proptest! {
    //         #[test]
    //         fn equivalent_to_rust_abs(i: ::value::Integer) {
    //             let mut backend = ::backend::Backend::new();

    //             let prog_src = format!("(abs {})", i);
    //             let prog = ::parse::parse(prog_src.as_bytes()).unwrap().1;
    //             let val = backend.eval_ast_list(prog).unwrap().unwrap();

    //             assert_eq!(val, Integer(i.abs()));
    //         }
    //     }
    // }
}



mod lists {
    use super::*;

    mod list {
        use super::*;

        eval_test!(list, Qexp(list![Integer(2), Integer(3)]), b"(list 2 3)");
    }

    mod car {
        use super::*;

        eval_test!(car_no_args, Value::Error(Error::ArityError), b"(car)");
        eval_test!(car_empty_list, Value::Error(Error::ArityError), b"(car '())");
        eval_test!(car_simple, Integer(1), b"(car '(1))");
    }

    mod cdr{
        use super::*;

        eval_test!(cdr_qexp, Qexp(vec![Integer(2), Integer(3)]), b"(cdr '(1 2 3))");

        // TODO: Validate this test with the standard
        eval_test!(cdr_nil, Value::Error(Error::ArityError), b"(cdr '())");

        eval_test!(cdr_no_args, Value::Error(Error::ArityError), b"(cdr)");

        eval_test!(cdr_single_arg, Qexp(vec![]), b"(cdr '(1))");
    }
}

mod eval {
    use super::*;

    eval_test!(eval, Integer(5), b"(eval '(+ 2 3))");
}

mod misc {
    use super::*;


    mod atom {
        use super::*;

        eval_test!(can_eval_integers, Integer(22), b"22");
        eval_test!(can_eval_symbols, Symbol("a".into()), b"'a");
        eval_test!(can_eval_qexp, Qexp(vec![Integer(1)]), b"'(1)");

        mod sexp {
            use super::*;

            // TODO: This is invalid Scheme, as described in the footnote
            // on page 9 in the R5RS standard
            //
            // Note: In many dialects of Lisp,  the empty combination, (),
            // is a legitimate expression.  In Scheme, combinations must have
            // at  least  one  subexpression,  so ()
            // is  not  a  syntactically  valid expression.
            eval_test!(empty_sexp, Sexp(list![]), b"()");
        }

        mod lambda {
            use super::*;

            eval_test!(lambda, Function {
                args: list![Identifier("hello".into())],
                body: list![
                    Sexp(list![Identifier("display".into()), Identifier("hello".into())])
                ],
            }, b"(lambda (hello) (display hello))");

            eval_test!(lambda_eval_inline, Integer(3), b"((lambda (a) (+ a 1)) 2)");
            eval_test!(lambda_eval_def, Integer(3),
                b"(define test (lambda (a) (+ a 1)))",
                b"(test 2)");

            eval_test!(lambda_rets_last, Integer(5),
                b"((lambda (a)
                        (+ a 1)
                        (+ a 3)) 2)");

            eval_test!(lambda_arity_error, Value::Error(Error::ArityError),
                b"((lambda (a) a) 2 3)");
        }
    }

    mod comment_and_whitespace {
        // use super::*;

        // eval_test!(test_rest, Integer(3), r#"
        //     ; this is a comment
        //     (+ 1 2) ; this is also a comment
        //     ; this too is a comment
        // "#.as_bytes());
    }

    mod fuzz {
        use super::*;

        eval_test!(fuzz_empty_lambda, Value::Error(Error::InvalidOperation), b"(())");

        eval_test!(fuzz_lookup_overflow, Value::Error(Error::InvalidOperation),
        b"((lambda (s) (s)) (s))");

        // eval_test!(fuzz_lookup_overflow2, Value::Error(Error::InvalidOperation),
        // b"((define a (define a ())))");


        eval_test!(fuzz_load_empty, Value::Error(Error::InvalidOperation), b"(load \"\")");
    }
}
