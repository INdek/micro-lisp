use value::{Value, Error};
use backend::Backend;
use list::List;
use num_integer::Integer;

macro_rules! vec_fold1 {
    ($list: expr, $op: expr) => {{
        let mut acc = $list.remove(0);
        for arg in $list.into_iter() {
            acc = $op(acc, arg);
        }
        acc
    }}
}

macro_rules! op {
    ($name: ident, $preevaluate: expr, $body: expr) => {
        pub fn $name(backend: &mut Backend, mut args: List<Value>) -> Value {
            if $preevaluate {
                args = args.into_iter()
                    .map(|v| backend.eval(v))
                    .collect();
            }

            // TODO: Make sure this is inlined
            let closure: &dyn Fn(&mut Backend, List<Value>) -> Value = &$body;

            closure(backend, args)
        }
    }
}


macro_rules! op_predicate {
    ($name: ident, $body: expr) => {
        pub fn $name(backend: &mut Backend, mut args: List<Value>) -> Value {
            args = args.into_iter()
                .map(|v| backend.eval(v))
                .collect();

            if args.len() != 1 {
                return Value::Error(Error::ArityError);
            }

            // TODO: Make sure this is inlined
            let closure: &dyn Fn(Value) -> Value = &$body;
            closure(args.remove(0))
        }
    }
}

macro_rules! ord_op {
    ($name: ident, $cmp: expr) => {
        op!($name, true, |_, ref mut args| {
            args.windows(2)
                .fold(Value::Boolean(true), |acc, slc| {
                    if let Value::Boolean(false) = acc {
                        return acc;
                    }

                    match slc {
                        &[Value::Integer(l), Value::Integer(r)] =>
                            Value::Boolean($cmp(l, r)),
                        &[Value::Boolean(l), Value::Boolean(r)] =>
                            Value::Boolean($cmp(l, r)),
                        &[Value::String(ref l), Value::String(ref r)] =>
                            Value::Boolean($cmp(l, r)),

                        &[ref err @ Value::Error(_), _] |
                            &[_, ref err @ Value::Error(_)] => err.clone(),

                        _ => Value::Error(Error::InvalidOperation),
                    }
                })
        });
    }
}


op!(op_quote, true, |_, mut args| {
    if args.len() != 1 {
        Value::Error(Error::ArityError)
    } else {
        args.remove(0)
    }
});

op!(op_sum, true, |_, mut args| {
    args.push(Value::Integer(0));
    vec_fold1!(args, |acc, val| acc + val)
});


op!(op_mul, true, |_, mut args| {
    args.push(Value::Integer(1));
    vec_fold1!(args, |acc, val| acc * val)
});

op!(op_sub, true, |_, mut args| {
    let args_len = args.len();
    match args_len {
        0 => Value::Error(Error::ArityError),
        // Perform unary negation if there is only one argument
        1 => -args.remove(0),
        _ => vec_fold1!(args, |acc, val| acc - val),
    }
});

op!(op_div, true, |_, mut args| {
    let args_len = args.len();
    match args_len {
        0 => Value::Error(Error::ArityError),
        _ => {
            args.push(Value::Integer(1));
            vec_fold1!(args, |acc, val| acc / val)
        }
    }
});

op!(op_eval, true, |backend, mut args| {
    if args.len() != 1 {
        return Value::Error(Error::ArityError);
    }

    let arg = args.remove(0);
    match arg {
        Value::Qexp(qexp_args) => backend.eval(Value::Sexp(qexp_args)),
        err @ Value::Error(_) => err,
        _ => Value::Error(Error::InvalidOperation),
    }
});

op!(op_car, true, |_, mut args| {
    if args.len() != 1 {
        return Value::Error(Error::ArityError);
    }

    let arg = args.remove(0);
    match arg {
        Value::Qexp(qargs) => qargs.get(0)
            .map(|val| val.clone())
            .unwrap_or(Value::Error(Error::ArityError)),
        err @ Value::Error(_) => err,
        _ => Value::Error(Error::InvalidOperation),
    }
});

op!(op_cdr, true, |_, mut args| {
    if args.len() != 1 {
        return Value::Error(Error::ArityError);
    }

    let arg = args.remove(0);
    match arg {
        Value::Qexp(ref qargs) if qargs.len() == 0 => Value::Error(Error::ArityError),
        Value::Qexp(mut qargs) => {
            qargs.remove(0);
            Value::Qexp(qargs)
        },
        err @ Value::Error(_) => err,
        _ => Value::Error(Error::InvalidOperation),
    }
});

op!(op_list, true, |_, args| {
    Value::Qexp(args)
});

op!(op_define, false, |backend, mut args| {
    if args.len() != 2 {
        return Value::Error(Error::ArityError);
    }

    let identifier = args.remove(0);
    let val = backend.eval(args.remove(0));

    match identifier.clone() {
        Value::Identifier(sname) => {
            backend.env.insert(sname, val);
            identifier
        },
        _ => Value::Error(Error::InvalidOperation),
    }
});


op!(op_lambda, false, |_, mut args| {
    if args.len() < 2 {
        return Value::Error(Error::ArityError);
    }

    let arg_names = args.remove(0);
    let body = args;

    match arg_names {
        Value::Sexp(names) => Value::Function {
            args: names,
            body,
        },
        _ => Value::Error(Error::InvalidOperation),
    }
});


ord_op!(op_gt, |l, r| l > r);
ord_op!(op_gte, |l, r| l >= r);
ord_op!(op_lt, |l, r| l < r);
ord_op!(op_lte, |l, r| l <= r);
ord_op!(op_eq, |l, r| l == r);

op_predicate!(op_odd, |arg| match arg {
    Value::Integer(i) => Value::Boolean(i.is_odd()),
    _ => Value::Error(Error::InvalidOperation),
});

op_predicate!(op_even, |arg| match arg {
    Value::Integer(i) => Value::Boolean(i.is_even()),
    _ => Value::Error(Error::InvalidOperation),
});

op!(op_max, true, |_backend, mut args| {
    if args.len() < 2 {
        return Value::Error(Error::ArityError);
    }

    vec_fold1!(args, |acc: Value, val| {
        if acc.is_error() {
            return acc;
        }

        match (&acc, &val) {
            (Value::Integer(a), Value::Integer(v)) if v > a => val,
            (Value::Integer(a), Value::Integer(v)) if v <= a => acc,
            _ => Value::Error(Error::InvalidOperation),
        }
    })
});

op!(op_min, true, |_backend, mut args| {
    if args.len() < 2 {
        return Value::Error(Error::ArityError);
    }

    vec_fold1!(args, |acc: Value, val| {
        if acc.is_error() {
            return acc;
        }

        match (&acc, &val) {
            (Value::Integer(a), Value::Integer(v)) if v < a => val,
            (Value::Integer(a), Value::Integer(v)) if v >= a => acc,
            _ => Value::Error(Error::InvalidOperation),
        }
    })
});



op!(op_if, false, |backend, mut args| {
    if args.len() < 2 {
        return Value::Error(Error::ArityError);
    }

    let cond = backend.eval(args.remove(0));
    let true_expr = args.remove(0);
    match cond {
        Value::Boolean(false) => args.into_iter()
            .fold(Value::Error(Error::InvalidOperation), |_, val| {
                backend.eval(val)
            }),
        _ => backend.eval(true_expr)
    }
});

// TODO: We might want to redirect output in nostd configurations
// TODO: Test preevaluate args
op!(op_print, true, |_, args| {
    output!("{}", args
        .iter()
        .map(|arg| format!("{}", arg))
        .collect::<Vec<String>>()
        .join(" "));
    Value::Qexp(vec![])
});

// TODO: Test preevaluate args
#[cfg(not(no_std))]
op!(op_load, true, |backend, args| {
    use std::io::Read;
    use std::fs::File;

    if args.len() != 1 {
        return Value::Error(Error::ArityError);
    }

    if let Value::String(ref path) = args[0] {

        let mut file_contents = String::new();
        let mut file = match File::open(path) {
            Ok(f) => f,
            Err(_) => return Value::Error(Error::InvalidOperation),
        };

        match file.read_to_string(&mut file_contents) {
            Err(_) => return Value::Error(Error::InvalidOperation),
            _ => {},
        };

        return match backend.eval_string(file_contents) {
            Err(_) => Value::Error(Error::InvalidOperation),
            Ok(None) => Value::Qexp(vec![]),
            Ok(Some(value)) => value,
        }
    } else {
        return Value::Error(Error::InvalidOperation);
    }
});


op!(op_equal, true, |_backend, mut args| {
    if args.len() != 2 {
        return Value::Error(Error::ArityError);
    }

    let (left, right) = (args.remove(0), args.remove(0));
    return Value::Boolean(left == right);
});

op!(op_quotient, true, |_backend, mut args| {
    if args.len() != 2 {
        return Value::Error(Error::ArityError);
    }

    let (numerator, denominator) = (args.remove(0), args.remove(0));
    match (numerator, denominator) {
        (_, Value::Integer(0)) => Value::Error(Error::InvalidOperation),
        (Value::Integer(numerator), Value::Integer(denominator)) => Value::Integer(numerator / denominator),
        (_, _) => Value::Error(Error::InvalidOperation),
    }
});