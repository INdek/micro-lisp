use core::{fmt, cmp};
use traits::*;

#[cfg(test)]
use proptest::prelude::*;


#[derive(Clone)]
#[cfg_attr(test, derive(Arbitrary))]
#[cfg_attr(test, proptest(filter("|x| x.denominator != 0")))]
pub struct Rational {
    numerator: i32,
    denominator: i32,
}

impl Rational {
    pub fn checked_new(numerator: i32, denominator: i32) -> Option<Self> {
        if denominator == 0 {
            None
        } else {
            Some(Self {
                numerator,
                denominator,
            })
        }
    }

    pub fn new(numerator: i32, denominator: i32) -> Self {
        Rational::checked_new(numerator, denominator)
            .expect("Created rational with zero denominator")
    }

    /// Flips a rational such that the numerator becomes the denominator
    /// and vice-versa
    pub fn flip(&self) -> Self {
        Self::new(self.denominator, self.numerator)
    }

    pub fn is_positive(&self) -> bool {
        (self.numerator > 0 && self.denominator > 0) || (self.numerator < 0 && self.denominator < 0)
    }

    pub fn is_negative(&self) -> bool {
        !self.is_positive()
    }

    pub fn reduce(&self) -> Self {
        use num_integer::Integer;

        let g = self.numerator.gcd(&self.denominator);

        let new = Self::new(self.numerator / g, self.denominator / g);
        if new.denominator < 0 {
            Self::new(0 - new.numerator, 0 - new.denominator)
        } else {
            new
        }
    }

    /// Returns the reciprocal
    pub fn recip(&self) -> Self {
        Rational::new(self.denominator, self.numerator)
    }
}

impl fmt::Display for Rational {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}/{}", self.numerator, self.denominator)
    }
}
impl fmt::Debug for Rational {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl cmp::Ord for Rational {
    /// The initial attempt at this algorithm used the n1*d2 == n2*d1 formula
    /// but that leads to integer overflow quickly.
    ///
    /// So we are trying the approach described here:
    /// https://janmr.com/blog/2014/05/comparing-rational-numbers-without-overflow/
    fn cmp(&self, other: &Rational) -> cmp::Ordering {
        let a = (self.numerator as i64) * (other.denominator as i64);
        let b = (other.numerator as i64) * (self.denominator as i64);

        let res = a.cmp(&b);
        let negative_denominator = (other.denominator < 0 && self.denominator >= 0) || (self.denominator < 0 && other.denominator >= 0);

        if negative_denominator {
            res.reverse()
        } else {
            res
        }
    }
}

impl cmp::PartialOrd for Rational {
    fn partial_cmp(&self, other: &Rational) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl cmp::Eq for Rational {}
impl cmp::PartialEq for Rational {
    fn eq(&self, other: &Rational) -> bool {
        self.cmp(other) == cmp::Ordering::Equal
    }
}


mod arith {
    use super::*;

    impl CheckedNeg for Rational {
        type Output = Rational;

        fn checked_neg(self) -> Option<Rational> {
            Some(Rational::checked_new(
                self.numerator.checked_neg()?,
                self.denominator,
            )?)
        }
    }
}

mod arith_i32 {
    use super::*;

    impl CheckedAdd<i32> for Rational {
        type Output = Rational;

        fn checked_add(self, rhs: i32) -> Option<Rational> {
            Some(Rational::checked_new(
                self.numerator.checked_add(rhs)?,
                self.denominator,
            )?)
        }
    }

    impl CheckedSub<i32> for Rational {
        type Output = Rational;

        fn checked_sub(self, rhs: i32) -> Option<Rational> {
            Some(Rational::checked_new(
                self.numerator.checked_sub(rhs)?,
                self.denominator,
            )?)
        }
    }

    impl CheckedMul<i32> for Rational {
        type Output = Rational;

        fn checked_mul(self, rhs: i32) -> Option<Rational> {
            Some(Rational::checked_new(
                self.numerator.checked_mul(rhs)?,
                self.denominator,
            )?)
        }
    }

    impl CheckedDiv<i32> for Rational {
        type Output = Rational;

        fn checked_div(self, rhs: i32) -> Option<Rational> {
            Some(Rational::checked_new(
                self.numerator,
                self.denominator.checked_mul(rhs)?,
            )?)
        }
    }

    impl CheckedRem<i32> for Rational {
        type Output = Rational;

        fn checked_rem(self, rhs: i32) -> Option<Rational> {
            Some(Rational::checked_new(
                self.numerator.checked_rem(rhs)?,
                self.denominator,
            )?)
        }
    }
}

mod arith_rational {
    use super::*;

    impl CheckedAdd<Rational> for Rational {
        type Output = Rational;

        // a/b + c/d = (a*d + b*c)/(b*d)
        fn checked_add(self, rhs: Rational) -> Option<Rational> {
            let ad = self.numerator.checked_mul(rhs.denominator)?;
            let bc = self.denominator.checked_mul(rhs.numerator)?;
            let bd = self.denominator.checked_mul(rhs.denominator)?;
            Some(Rational::checked_new(ad.checked_add(bc)?, bd)?)
        }
    }

    impl CheckedSub<Rational> for Rational {
        type Output = Rational;

        fn checked_sub(self, rhs: Rational) -> Option<Rational> {
            let ad = self.numerator.checked_mul(rhs.denominator)?;
            let bc = self.denominator.checked_mul(rhs.numerator)?;
            let bd = self.denominator.checked_mul(rhs.denominator)?;
            Some(Rational::checked_new(ad.checked_sub(bc)?, bd)?)
        }
    }

    impl CheckedMul<Rational> for Rational {
        type Output = Rational;

        fn checked_mul(self, rhs: Rational) -> Option<Rational> {
            Some(Rational::checked_new(
                self.numerator.checked_mul(rhs.numerator)?,
                self.denominator.checked_mul(rhs.denominator)?
            )?)
        }
    }

    impl CheckedDiv<Rational> for Rational {
        type Output = Rational;

        fn checked_div(self, rhs: Rational) -> Option<Rational> {
            let bc = self.denominator.checked_mul(rhs.numerator)?;

            match bc {
                0 => None,
                _ => Some(Rational::checked_new(self.numerator.checked_mul(rhs.denominator)?, bc)?),
            }
        }
    }

    impl CheckedRem<Rational> for Rational {
        type Output = Rational;

        fn checked_rem(self, rhs: Rational) -> Option<Rational> {
            let ad = self.numerator.checked_mul(rhs.denominator)?;
            let bc = self.denominator.checked_mul(rhs.numerator)?;
            let bd = self.denominator.checked_mul(rhs.denominator)?;
            Some(Rational::checked_new(ad.checked_rem(bc)?, bd)?)
        }
    }
}


impl From<Rational> for f64 {
    fn from(r: Rational) -> Self {
        (r.numerator as f64) / (r.denominator as f64)
    }
}

impl From<Rational> for f32 {
    fn from(r: Rational) -> Self {
        (r.numerator as f32) / (r.denominator as f32)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! rational {
        ($num: expr, $denom: expr) => {
            Rational::new($num, $denom)
        };
    }

    #[test]
    #[should_panic]
    fn prevents_invalid_rationals() {
        rational!(0, 0);
    }

    #[test]
    #[should_panic]
    fn flip_panics() {
        rational!(0, 1).flip();
    }


    #[test]
    fn display_implementation() {
        assert_eq!(format!("{}", rational!(1, 1)), "1/1");
        assert_eq!(format!("{}", rational!(1, 10)), "1/10");
    }


    #[test]
    fn partial_eq() {
        assert_eq!(rational!(1, 1), rational!(10, 10));

        assert_eq!(rational!(0, 1), rational!(0, 1));
        assert_eq!(rational!(0, 1), rational!(0, 10));

        assert_eq!(rational!(10, 100), rational!(100, 1000));

        assert_eq!(rational!(i32::max_value(), i32::max_value()), rational!(i32::max_value(), i32::max_value()));
        assert_eq!(rational!(i32::min_value(), i32::min_value()), rational!(i32::min_value(), i32::min_value()));

        assert_eq!(rational!(1, 1), rational!(-1, -1));
    }


    #[test]
    fn cmp() {
        assert!(rational!(2, 1) > rational!(1, 1));
        assert!(rational!(1, 1) < rational!(2, 1));

        assert!(rational!(1, 2) < rational!(1, 1));
        assert!(rational!(99, 88) > rational!(99, 89));

        assert!(rational!(1, 1) > rational!(-1, 1));
    }

    #[test]
    fn cmp_negative() {
        assert!(rational!(1, 1) > rational!(1, -1));
        assert!(rational!(1, -1) < rational!(1, 1));
    }


    #[test]
    fn positive_negative() {
        assert!(rational!(1, 1).is_positive());
        assert!(!rational!(1, 1).is_negative());

        assert!(!rational!(-1, 1).is_positive());
        assert!(rational!(!1, 1).is_negative());

        assert!(!rational!(1, -1).is_positive());
        assert!(rational!(1, -1).is_negative());

        assert!(rational!(-1, -1).is_positive());
        assert!(!rational!(-1, -1).is_negative());
    }


    #[test]
    fn reduce() {
        let t1 = rational!(100, 10).reduce();
        assert_eq!(t1.numerator, 10);
        assert_eq!(t1.denominator, 1);

        let t2 = rational!(120, 90).reduce();
        assert_eq!(t2.numerator, 4);
        assert_eq!(t2.denominator, 3);
    }


    proptest! {
        #[test]
        fn unity_partial_eq(r: Rational) {
            assert_eq!(r, r);
        }

        #[test]
        fn flip(a: i32, b: i32) {
            if a == 0 || b == 0 { return Ok(()) }

            assert_eq!(rational!(a, b).flip(), rational!(b, a));
        }

        #[test]
        fn partial_cmp_is_cmp(a: i32, b: i32) {
            if b == 0 { return Ok(()) }

            let left = rational!(a, b);
            let right = rational!(a, b);
            assert_eq!(Some(left.cmp(&right)), left.partial_cmp(&right))
        }

        #[test]
        fn comp_with_fp(l: Rational, r: Rational) {
            let left_fp: f64 = l.clone().into();
            let right_fp: f64 = r.clone().into();

            assert_eq!(l.partial_cmp(&r), left_fp.partial_cmp(&right_fp))
        }

        #[test]
        fn no_crash(r: Rational) {
            r.flip();
            r.reduce();
            r.recip();
        }


        #[test]
        fn reduce_does_not_change_value(r: Rational) {
            assert_eq!(r, r.reduce());
        }

        #[test]
        fn reduce_is_stable(r: Rational) {
            let r1 = r.reduce();
            let r2 = r1.reduce();

            assert_eq!(r1.numerator, r2.numerator);
            assert_eq!(r1.denominator, r2.denominator);
        }

        #[test]
        fn reciprocal_should_be_equivalent_to_inverting_numbers(a: i32, b: i32) {
            if a == 0 || b == 0 { return Ok(()) }

            assert_eq!(rational!(a, b).recip(), rational!(b, a));
        }
    }
}