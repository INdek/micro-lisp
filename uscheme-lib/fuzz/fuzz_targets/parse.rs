#![no_main]
#[macro_use] extern crate libfuzzer_sys;
extern crate uscheme_lib;

fuzz_target!(|data: &[u8]| {
    let _ = uscheme_lib::parse::parse(data);
});
