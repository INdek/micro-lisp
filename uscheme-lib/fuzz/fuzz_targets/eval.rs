#![no_main]
#[macro_use] extern crate libfuzzer_sys;
extern crate uscheme_lib;

fuzz_target!(|data: &[u8]| {
    let ast_res = uscheme_lib::parse::parse(data);
    if let Ok(ast) = ast_res {
        let mut backend = uscheme_lib::backend::Backend::new();

        let _ = backend.eval_ast_list(ast.1);
    }
});
